from typing import List, NoReturn
from collections import Counter
import logging


class MaximizeBoxes:
    generated_boxes = []  # get the generated boxes possibility
    temp_array = []  # used to help generating integer decomposition

    def __init__(self, sequence: str, capacity=10):
        self.sequence = sequence
        self.capacity = capacity

    def optimize_boxes(self) -> NoReturn:
        """
            Main function to solve the maximize boxes problem
            Loop over the boxes possibilities and get
        """
        # transform the string as list of int
        items = [int(s) for s in self.sequence]

        optimized_boxes: List = self.find_solution(items, capacity=self.capacity)

        self.pretty_print(optimized_boxes)

    def find_sum_way(self, capacity, array, memo={}) -> NoReturn:
        """
            Find the sum decomposition for a given positive integer , feel the global variable generated_boxes
            which contain all the decomposition of the integer within a list
        """
        if len(array) < capacity - 1:
            for i in range(1, capacity + 1):
                self.find_sum_way(capacity - i, array + [i])
        else:
            if capacity == 0:
                sorted_box = array
            else:
                sorted_box = array + [capacity]
            sorted_box.sort()
            elt = str(sorted_box)
            # memoize with the string literal list sorted so we don't count [1, 2] & [2, 1] as a different combination
            if elt not in memo:
                memo[elt] = sorted_box
                self.generated_boxes.insert(0, sorted_box)

    @staticmethod
    def contain_enough_box(counter: Counter, box: List[int]) -> bool:
        """ Tells whether we can consume a box from our current boxes list stored in Counter object """
        box_counter = Counter(box)
        for capacity in box:
            if capacity not in counter or box_counter[capacity] > counter[capacity] or counter[capacity] <= 0:
                return False
        return True

    @staticmethod
    def remaining(counter: Counter) -> List[List[int]]:
        """ aggregate the remaining item which have not been packed yet"""
        boxes = []
        for k, v in counter.items():
            for _ in range(v):
                boxes.append([k])
        return boxes

    def find_solution(self, items: List[int], capacity: int = 10):
        """
            Loop over every decomposition of integer from 10 down to 1 if needed
            find the box that match the available items stored into Counter object
            which is decremented as we store new boxes.
         """
        box_result = []

        counter = Counter(items)
        while capacity > 1:
            self.find_sum_way(capacity, self.temp_array)
            logging.debug(f"Generated possibility for capacity: {capacity} ")
            logging.debug(self.generated_boxes)

            for g_box in self.generated_boxes:
                while self.contain_enough_box(counter, g_box):
                    box_result.append(g_box)
                    for item in g_box:
                        counter[item] -= 1
                        if counter[item] <= 0:
                            del counter[item]
            # clear lists and decrement capacity
            capacity -= 1
            self.generated_boxes.clear()
            self.temp_array.clear()

        final_boxes_list = box_result + self.remaining(counter)
        logging.info(final_boxes_list)
        logging.debug(counter)
        return final_boxes_list

    @staticmethod
    def pretty_print(boxes: List[List[int]]) -> None:
        """ Display the result with the following format XXX/XX/X/XXX => X """
        str_res = ""
        for box in boxes:
            string_ints = [str(i) for i in box]
            str_of_ints = "".join(string_ints) + "/"
            str_res += str_of_ints

        print(f"{str_res[:-1]} => {len(boxes)}")

