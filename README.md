# Requirement:

python 3

# How To Run ?:

```
python main.py
optional arguments:
  -h, --help            show this help message and exit
  --sequence [SEQUENCE] sequence of the articles
```
**Exeample:** python main.py --sequence "123776524"
