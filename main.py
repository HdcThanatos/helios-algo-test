from maximize_box import MaximizeBoxes
import sys
import argparse


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description='Optimize box packaging.')
    arg_parser.add_argument('--sequence',
                            nargs='?',
                            type=str,
                            help='sequence of the articles',
                            required=False,
                            default="163841689525773")

    args = arg_parser.parse_args()

    sequence: str = args.sequence

    maximize_box = MaximizeBoxes(sequence)
    maximize_box.optimize_boxes()
